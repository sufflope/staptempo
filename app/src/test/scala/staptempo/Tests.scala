package staptempo

import java.time.Instant
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.{MustMatchers, OptionValues, TryValues, WordSpec}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class Tests extends WordSpec with GeneratorDrivenPropertyChecks with MustMatchers with OptionValues with TryValues {

  import Tests._

  "staptempo" when {

    "receiving taps" should {

      "discard previous taps older than reset-time" in {
        forAll { test: ResetTimeTest ⇒
          val app = STapTempo.pureapp(Array("-r", test.resetTime.toString, "-s", test.kept.toString)).value
          val result = test.taps
            .foldLeft(Seq.empty[Instant]) { case (model, instant) ⇒ app.update(Message.Valid.Tap(instant), model)._1 }
          result.size must be(test.kept)
        }
      }

      "only keep sample-size taps at most" in {
        forAll { test: SampleSizeTest ⇒
          val app = STapTempo.pureapp(Array("-r", Int.MaxValue.toString, "-s", test.sampleSize.toString)).value
          val result = test.taps
            .foldLeft(Seq.empty[Instant]) { case (model, instant) ⇒ app.update(Message.Valid.Tap(instant), model)._1 }
          result.size must be(test.taps.size min test.sampleSize)
        }
      }

    }

  }

}

object Tests {

  implicit val arbInstant: Arbitrary[Instant] = Arbitrary(Gen.calendar.map(_.toInstant))

  final case class ResetTimeTest(resetTime: Int, kept: Int, taps: Seq[Instant])

  implicit val arbResetTimeTest: Arbitrary[ResetTimeTest] = Arbitrary(
    for {
      resetTime ← Gen.posNum[Int]
      first     ← Arbitrary.arbitrary[Instant]
      valid     ← Gen.listOf(Gen.chooseNum[Long](0, resetTime.toLong - 1).map(first.plusSeconds))
      invalid   ← Gen.listOf(Gen.posNum[Long].map(first.minusSeconds))
    } yield ResetTimeTest(resetTime, valid.size + 1, (valid ++ invalid :+ first).sorted)
  )

  final case class SampleSizeTest(sampleSize: Int, taps: Seq[Instant])

  implicit val arbSampleSizeTest: Arbitrary[SampleSizeTest] = Arbitrary(
    for {
      sampleSize ← Gen.sized(size ⇒ Gen.chooseNum(0, size))
      taps       ← Gen.sequence[Seq[Instant], Instant](Some(Arbitrary.arbitrary[Instant]))
    } yield SampleSizeTest(sampleSize, taps)
  )

}
