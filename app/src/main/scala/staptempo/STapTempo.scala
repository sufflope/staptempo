package staptempo

import cats.effect.IO
import eu.timepit.refined.auto._
import java.time.{Duration, Instant}
import pureapp.{PureApp, Terminal}

trait STapTempo extends PureApp {
  override type Model = Seq[Instant]
  override type Msg   = Message
  override type Cmd   = Option[Command]
}

object STapTempo {

  def pureapp(args: Array[String]): Option[STapTempo] =
    for (Args(precision, resetTime, sampleSize) ← Args(args))
      yield
        new STapTempo {

          override def init = (Nil, Some(Command.Info("Hit enter key for each beat (q to quit).\n")))

          override def io(model: Model, command: Cmd): IO[Message] =
            for {
              _ ← command match {
                   case Some(Command.Info(message)) ⇒ Terminal.putStr(message)
                   case Some(Command.Tempo(bpm))    ⇒ Terminal.putStr(s"Tempo: ${s"%.${precision}f".format(bpm)} bpm ")
                   case _                           ⇒ IO.unit
                 }
              input ← Terminal.readLine
            } yield {
              input match {
                case ""  ⇒ Message.Valid.Tap(Instant.now)
                case "q" ⇒ Message.Valid.Quit
                case _   ⇒ Message.Invalid
              }
            }

          override def update(message: Message, model: Model) =
            message match {
              case Message.Valid.Tap(now) ⇒
                val beats =
                  (now +: model)
                    .take(sampleSize)
                    .filter(beat ⇒ Duration.between(beat, now).getSeconds < resetTime)
                beats match {
                  case latest :: (_ :+ oldest) ⇒
                    val duration = Duration.between(oldest, latest)
                    (
                      beats,
                      Some(
                        Command.Tempo(
                          ((beats.size - 1) * 60) /
                            (duration.getSeconds.toDouble + duration.getNano.toDouble / (1000 * 1000 * 1000L))
                        )
                      )
                    )
                  case _ ⇒
                    (beats, Some(Command.Info("[Hit enter key one more time to start bpm computation...]")))
                }
              case _ ⇒ (model, None)
            }

          override val quit: Option[Message] = Some(Message.Valid.Quit)

          sys.addShutdownHook(println("Bye Bye!"))

        }

}
