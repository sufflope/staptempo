package staptempo

import java.time.Instant

sealed trait Message extends Product with Serializable

object Message {

  sealed trait Valid extends Message

  object Valid {
    case object Quit                       extends Valid
    final case class Tap(instant: Instant) extends Valid
  }

  case object Invalid extends Message

}
