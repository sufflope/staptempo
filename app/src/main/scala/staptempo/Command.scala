package staptempo

sealed trait Command extends Product with Serializable

object Command {

  final case class Info(value: String)  extends Command
  final case class Tempo(value: Double) extends Command

}
