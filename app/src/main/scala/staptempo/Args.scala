package staptempo

import buildinfo.BuildInfo
import eu.timepit.refined.{refineV, W}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import eu.timepit.refined.numeric.{Interval, NonNegative}
import scopt.{OptionParser, Read}

@SuppressWarnings(Array("org.wartremover.warts.DefaultArguments"))
final case class Args(precision: Args.Precision = 0, resetTime: Args.Positive = 5, sampleSize: Args.Positive = 5)

object Args {

  private type PositiveConstraints = NonNegative
  private type Positive            = Int Refined PositiveConstraints

  private type PrecisionConstraints = Interval.Closed[W.`0`.T, W.`5`.T]
  private type Precision            = Int Refined PrecisionConstraints

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  implicit val positiveRead: Read[Positive] = Read.intRead.map(
    i ⇒
      refineV[PositiveConstraints](i).getOrElse(throw new IllegalArgumentException(s"'${i}' is not a positive number"))
  )

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  implicit val precisionRead: Read[Precision] = Read.intRead.map(
    i ⇒
      refineV[PrecisionConstraints](i)
        .getOrElse(throw new IllegalArgumentException(s"'${i}' is not a number between 0 and 5"))
  )

  def apply(args: Array[String]): Option[Args] = parser.parse(args, Args())

  private val parser: OptionParser[Args] = new OptionParser[Args](BuildInfo.moduleName) {

    head(BuildInfo.moduleName, BuildInfo.version)

    help("help").abbr("h").text("display this help message")

    opt[Precision]('p', "precision")
      .action((p, args) ⇒ args.copy(precision = p))
      .text("set the decimal precision of the tempo display; default is 0 digits, max is 5 digits")

    opt[Positive]('r', "reset-time")
      .action((r, args) ⇒ args.copy(resetTime = r))
      .text("set the time in second to reset the computation; default is 5 seconds")

    opt[Positive]('s', "sample-size")
      .action((s, args) ⇒ args.copy(sampleSize = s))
      .text("set the number of samples needed to compute the tempo; default is 5 samples")

    version("version").abbr("v").text("display the version")

  }

}
