# staptempo - a Scala port of taptempo

Inspired by, and intended to be a Scala clone of, [taptempo](https://taptempo.tuxfamily.org). See [the french presentation of the original project](https://linuxfr.org/users/mzf/journaux/un-tap-tempo-en-ligne-de-commande).

## Requirements

You need Java 8 and a version of [sbt](https://www.scala-sbt.org/download.html) able to boostrap the actual version used in this project (`1.1.6`), so most probably `sbt 1.x`.

## Useful commands

### Start sbt REPL

Simply type `sbt`. All subsequent commands assume you are in an `sbt` session.

### Codestyle

Format automatically the code according to `scalafmt` stylesheet using `scalafmtAll`. You can check violations using `scalafmtCheck`.

### Tests

Run the tests, codestyle verifications and generate code coverage reports with `validate`. Code coverage reports will be in `target/scala-2.12/scoverage-report`.

### Packaging

Generate a `jar` with `assembly`. It will be generated at `app/target/scala-2.12/staptempo-<version>.jar`.

## Known limitations

Currently staptempo is not internationalized.
