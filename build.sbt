lazy val root = Project(id = "staptempo", base = file("."))
  .settings(moduleName := "root")
  .enablePlugins(UnpublishedPlugin)
  .aggregate(app)

lazy val app = project
  .settings(
    assemblyJarName := s"${name.value}-${version.value}.jar",
    buildInfoKeys   := Seq[BuildInfoKey](BuildInfoKey(moduleName), version),
    moduleName      := "Tap Tempo",
    name            := "staptempo"
  )
  .enablePlugins(BuildInfoPlugin, GitVersioning, StrictPlugin, UnpublishedPlugin)
  .settings(
    libraryDependencies ++= Seq(
      "com.github.scopt" %% "scopt"      % Versions.scopt,
      "eu.timepit"       %% "refined"    % Versions.refined,
      "org.scalacheck"   %% "scalacheck" % Versions.scalacheck % Test,
      "org.scalatest"    %% "scalatest"  % Versions.scalatest % Test
    )
  )
  .dependsOn(pureapp)

lazy val pureapp = project
  .settings(
    scalacOptions --= Seq("-Ywarn-unused:params", "-Ywarn-value-discard")
  )
  .enablePlugins(UnpublishedPlugin)
