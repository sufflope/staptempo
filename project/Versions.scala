object Versions {
  val refined    = "0.9.2"
  val scalacheck = "1.14.0"
  val scalatest  = "3.0.5"
  val scopt      = "3.7.0"
}
