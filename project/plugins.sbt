addSbtPlugin("com.eed3si9n" % "sbt-assembly"          % "0.14.7")
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo"         % "0.9.0")
addSbtPlugin("com.nrinaudo" % "kantan.sbt"            % "2.2.0")
addSbtPlugin("com.nrinaudo" % "kantan.sbt-release"    % "2.2.0")
addSbtPlugin("com.nrinaudo" % "kantan.sbt-scalafmt"   % "2.2.0")
addSbtPlugin("com.nrinaudo" % "kantan.sbt-scalastyle" % "2.2.0")
